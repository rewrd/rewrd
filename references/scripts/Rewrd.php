<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */
namespace rewrd;

array_walk(glob(dirname(realpath(__FILE__)).DIRECTORY_SEPARATOR.'*.php'), function($file, $index) {
	include_once $file;
});

?>