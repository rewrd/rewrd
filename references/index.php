<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */
ini_set('error_reporting', E_ERROR);ini_set('display_errors', true);

date_default_timezone_set('UTC');

require_once 'vendor/WindowsAzure/WindowsAzure.php';

require_once 'vendor/AnamoPHPFramework.4.phar';

require_once 'vendor/EmberPHP.phar';

?>