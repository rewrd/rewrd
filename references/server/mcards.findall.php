<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */

if(isset($params['ids'])) {
	$sql1 = $db1->q("SELECT *
FROM `mcards`
WHERE `id` IN ('".implode("','", $params['ids'])."');");
} else {
	$sql1 = $db1->q("SELECT 
    `fiats`.`face_value` AS `card_face_value`,
    FLOOR(`tb1`.`total_sale_now` / `fiats`.`face_value`) AS `cards`,
    IFNULL(`tb1`.`total_volume` / FLOOR(`tb1`.`total_sale_now` / `fiats`.`face_value`),
            0) AS `coins`
FROM
    `fiats`
        LEFT JOIN
    (SELECT 
        `view_trades_sales_now`.`now_currency_id`,
            ROUND(SUM(`sale_now`), 7) AS `total_sale_now`,
            ROUND(SUM(`volume`), 7) AS `total_volume`
    FROM
        (SELECT @P1:=%d p) AS `parm`, `view_trades_sales_now`) AS `tb1` ON (`tb1`.`now_currency_id` = `fiats`.`currency_id`)
WHERE
    `fiats`.`currency_id` = %d


;", 840, 840);
}

$json['meta']['total'] = $sql1->num_rows;

$json['mcards'] = $Rewrd->Mcard->reflect($sql1->fetch_all_objects());

?>