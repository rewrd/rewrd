<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */

if(isset($params['ids'])) {
	$sql1 = $db1->q("SELECT *
FROM `currencies`
WHERE `id` IN (".implode(",", array_map('intval', $params['ids'])).")
AND `id` <> 999;");
} else {
	$sql1 = $db1->q("SELECT *
FROM `currencies`
WHERE `id` <> 999;");
}

$json['meta']['total'] = $sql1->num_rows;

$json['currencies'] = $Rewrd->Currency->reflect($sql1->fetch_all_objects());

?>