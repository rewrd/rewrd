<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */

$dbObject = $db1->table('order_claim_efforts')->findOne(array('id' => $id));
if(empty($dbObject)) {
	throw new Exception();
}

echo json_encode(array(
	'order-claim-effort' => $Rewrd->OrderClaimEffort->reflect($dbObject)
));

?>