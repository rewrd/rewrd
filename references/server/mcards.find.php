<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */

$dbObject = $db1->table('mcards')->findOne(array('id' => $id));
if(empty($dbObject)) {
	throw new Exception();
}

echo json_encode(array(
	'mcard' => $Rewrd->Mcard->reflect($dbObject)
));

?>