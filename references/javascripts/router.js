Rewrd.Router.reopen({
	location: 'auto',
});
Rewrd.Router.map(function() {
	this.route('help');
	this.route('press');
	this.route('for-companies');
	this.route('about-us');
	this.route('bitcoin');
	this.route('card', {path: ':card_id/card'});
	this.route('carts');
	this.route('accounts');
	this.route('orders');
	this.route('privacy-and-terms');
	this.route('404', {path: '*:'});
});