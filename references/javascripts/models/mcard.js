Rewrd.Mcard = Rewrd.Mcard.extend({
	satoshi: function() {
		return this.get('volume') / 0.00000001;
	}.property('volume'),
	
	isUnavailable: function() {
		return this.get('qty') == 0;
	}.property('qty'),
});