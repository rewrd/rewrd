<?php /* Copyright 2006-2013 Panagiotis Tsimpoglou. All rights reserved. */

$GLOBALS['Rewrd']->OrderClaim = DS\Model::extend(array(
	'id' => DS\attr('number', array(
		'db' => 'id',
		'noupdatecreate' => true
	)),
	'dateAdded' => DS\attr('date', array(
		'db' => 'datea',
		'noupdatecreate' => true
	)),
	'ip' => DS\attr('number', array(
		'db' => 'ip',
		'noupdatecreate' => true
	)),
	'fingerprint' => DS\attr('string', array(
		'db' => 'fingerprint',
		'noupdatecreate' => true
	)),
	'wallet' => DS\attr('string', array(
		'db' => 'wallet',
		'noupdatecreate' => true
	)),
));

?>