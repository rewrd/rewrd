<?php /* Copyright 2006-2013 Panagiotis Tsimpoglou. All rights reserved. */

$GLOBALS['Rewrd']->Mcard = DS\Model::extend(array(
	'id' => DS\attr('string', array(
		'db' => 'card_face_value',
		'noupdatecreate' => true
	)),
	'volume' => DS\attr('number', array(
		'db' => 'coins',
		'noupdatecreate' => true
	)),
	'amount' => DS\attr('number', array(
		'db' => 'card_face_value',
		'noupdatecreate' => true
	)),
	'qty' => DS\attr('number', array(
		'db' => 'cards',
		'noupdatecreate' => true
	)),
));

?>