<?php /* Copyright 2006-2013 Panagiotis Tsimpoglou. All rights reserved. */

$GLOBALS['Rewrd']->Currency = DS\Model::extend(array(
	'id' => DS\attr('number', array(
		'db' => 'id',
		'noupdatecreate' => true
	)),
	'isoCode' => DS\attr('string', array(
		'db' => 'code',
		'noupdatecreate' => true
	)),
	'multiplier' => DS\attr('number', array(
		'db' => 'multiplier',
		'noupdatecreate' => true
	)),
));

?>