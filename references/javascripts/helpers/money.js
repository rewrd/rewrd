function(value, region, options) {
	if(Em.isEmpty(region)) {
		return new Handlebars.SafeString('<span>&hellip;</span>');
	}
  return new Handlebars.SafeString('<span>' + accounting.formatMoney(value, {symbol: region.symbol, format: region.format, decimal: region.decimal, thousand: region.thousand, precision: region.precision}) + '</span>');
}