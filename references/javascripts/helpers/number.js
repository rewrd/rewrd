function(value, options) {
  if(!Em.isEmpty(options.hash.multiplier)) {
		value *= new Number(options.hash.multiplier);
	}
  return new Handlebars.SafeString('<span>' + accounting.formatNumber(value) + '</span>');
}