function(value, vcurrency, region, options) {
  if(Em.isEmpty(vcurrency) || Em.isEmpty(region)) {
		return new Handlebars.SafeString('<span>&hellip;</span>');
	}
	numeral.language('fr', {
			delimiters: {
					thousands: (region.thousand == region.decimal? ' ': region.thousand),
					decimal: region.decimal
			},
			abbreviations: {
					thousand: 'k',
					million: 'm',
					billion: 'b',
					trillion: 't'
			},
			ordinal : function (number) {
					return number === 1 ? 'er' : 'ème';
			},
			currency: {
					symbol: '€'
			}
	});
	
	// switch between languages
	numeral.language('fr');console.log(9999, '0'+(region.thousand == region.decimal? ' ': region.thousand)+'0'+region.decimal+'0000[000000]');
	
  return new Handlebars.SafeString('<span style="font-family:\'Source Code Pro\'">' + (value == 0? 0: numeral(value * vcurrency.multiplier).format('0'+(region.thousand == region.decimal? ' ': region.thousand)+'0'+region.decimal+'0000[000000]') + vcurrency.trailer) + '</span>');
}