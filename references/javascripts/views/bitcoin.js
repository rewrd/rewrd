Em.View.extend({
	tagName: 'div',
	
	classNames: ['page-section-1'],
	
	hasOfferModal: function() {
		return !Em.isEmpty(this.get('controller.offer'));
	}.property('controller.offer'),
	hasOfferModalChanged: function() {
		if(!Em.isEmpty(Em.View.views['application-view'])) {
			Em.View.views['application-view'].set('hasModal', this.get('hasOfferModal'));
		}
		if(this.get('hasOfferModal')) {
			$('#offerModal')
			.css('display', 'block');
			$('.modal-backdrop')
			.one('transitionend mozTransitionEnd msTransitionEnd oTransitionEnd webkitTransitionEnd', function() {
				$('#offerModal').addClass('in');
			});
		} else {
			$('#offerModal')
			.removeClass('in');
		}
	}.observes('controller.offer'),
	
	hasRatesModal: false,
	hasRatesModalChanged: function() {
		if(!Em.isEmpty(Em.View.views['application-view'])) {
			Em.View.views['application-view'].set('hasModal', this.get('hasRatesModal'));
		}
		if(this.get('hasRatesModal')) {
			$('#hours-modal')
			.css('display', 'block');
			$('.modal-backdrop')
			.one('transitionend mozTransitionEnd msTransitionEnd oTransitionEnd webkitTransitionEnd', function() {
				$('#hours-modal').addClass('in');
			});
		} else {
			$('#hours-modal')
			.removeClass('in');
		}
	}.observes('hasRatesModal'),
	
	didInsertElement: function() {
		var t = this;
		$('#hours-modal')
		.on('transitionend mozTransitionEnd msTransitionEnd oTransitionEnd webkitTransitionEnd', function() {
			if(!t.get('hasRatesModal')) {
				$(this).css('display', 'none');
			}
		});
		$('#offerModal')
		.on('transitionend mozTransitionEnd msTransitionEnd oTransitionEnd webkitTransitionEnd', function() {
			if(!t.get('hasOfferModal')) {
				$(this).css('display', 'none');
			}
		});
	},
	
	click: function() {
		this.set('hasRatesModal', false);
		this.set('controller.offer', null);
	},
	
	actions: {
		toggleRatesModal: function() {
			this.set('hasRatesModal', !this.get('hasRatesModal'));
		},
		
		showOfferModal: function(context) {
			this.set('controller.offer', context);
		},
		hideOfferModal: function() {
			this.set('controller.offer', null);
		},
	}
});