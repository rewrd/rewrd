Em.View.extend({
	tagName: 'div',
	
	elementId: 'application-view',
	
	selectCoin: false,
	selectedCoin: 'Bitcoin',
	
	hasRegions: function() {
		return this.get('controller.regions.length') > 0;
	}.property('controller.regions'),
	
	selectCurrency: false,
	selectCurrencyChanged: function() {
		if(this.get('selectCurrency')) {
			Em.run.next(this, function() {
				$('#delivery-time-form ul:first li:first a').focus();
			});
		}
	}.observes('selectCurrency'),
	selectedCurrencyIsEUR: Em.computed.equal('selectCurrency', 'EUR'),
	selectedCurrencyIsUSD: Em.computed.equal('selectCurrency', 'USD'),
	selectedCurrencyIsGBP: Em.computed.equal('selectCurrency', 'GBP'),
	
	selectVCurrency: false,
	selectVCurrencyChanged: function() {
		if(this.get('selectVCurrency')) {
			Em.run.next(this, function() {
				$('#delivery-time-form ul:last li:first a').focus();
			});
		}
	}.observes('selectVCurrency'),
	
	selectRegion: false,
	selectRegionChanged: function() {
		if(this.get('selectRegion')) {
			Em.run.next(this, function() {
				$('#delivery-time-form ul:last li:first a').focus();
			});
		}
	}.observes('Region'),
	
	hasModal: false,
	hasModalChanged: function() {
		if(this.get('hasModal')) {
			$('body')
			.addClass('modal-open')
			.append('<div class="modal-backdrop fade"></div>');
			Em.run.later(this, function() {
				$('.modal-backdrop').addClass('in');
			}, 1);
		} else {
			$('.modal-backdrop')
			.one('transitionend mozTransitionEnd msTransitionEnd oTransitionEnd webkitTransitionEnd', function() {
				$(this).remove();
				$('body').removeClass('modal-open');
			})
			.removeClass('in');
		}
	}.observes('hasModal'),
	
	click: function() {
		this.set('selectCoin', false);
		this.set('selectCurrency', false);
	},
	
	didInsertElement: function() {
		if(Em.isEmpty(this.get('controller.selectedVCurrency'))) {
			this.set('controller.selectedVCurrency', this.get('controller.vCurrencies.firstObject'));
		}
	},
	
	actions: {
		toggleSelectCoin: function() {
			this.set('selectCoin', !this.get('selectCoin'));
		},
		
		toggleSelectCurrency: function() {
			this.set('selectCurrency', !this.get('selectCurrency'));
		},
		
		selectCurrency: function(context) {
			this.set('controller.selectedCurrency', context);
			this.send('toggleSelectCurrency');
		},
		
		toggleSelectVCurrency: function() {
			this.set('selectVCurrency', !this.get('selectVCurrency'));
		},
		
		toggleSelectRegion: function() {
			this.set('selectRegion', !this.get('selectRegion'));
		},
		
		selectVCurrency: function(context) {
			this.set('controller.selectedVCurrency', context);
			this.send('toggleSelectVCurrency');
		},
		
		selectRegion: function(context) {
			this.set('controller.selectedRegion', context);
			this.send('toggleSelectRegion');
		},
	}
});