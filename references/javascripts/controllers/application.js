Em.Controller.extend({
	currencies: null,
	currenciesChanged: function() {
		if(this.get('currencies.length') > 0 && Em.isEmpty(this.get('selectedCurrency'))) {
			this.set('selectedCurrency', this.get('currencies.firstObject'));
		}
	}.observes('currencies.@each'),
	
	selectedCurrency: null,
	selectedCurrencyChanged: function() {
		this.set('regions', $.parseJSON($('meta[name="rw:currencies"]').attr('content')).filterBy('iso', this.get('selectedCurrency.isoCode')));
	}.observes('selectedCurrency'),
	
	regions: null,
	regionsChanged: function() {
		if(this.get('regions.length') > 0) {
			this.set('selectedRegion', this.get('regions.firstObject'));
		}
	}.observes('regions.@each'),
	selectedRegion: null,
	
	vCurrencies: Ember.A([ // http://bitcoin.stackexchange.com/questions/114/what-is-a-satoshi
		{id: 'BTC', title: 'BTC (Bitcoin)', multiplier: 1, trailer: ' BTC'},
		{id: 'Satoshis', title: 'Satoshi (0.00000001 BTC)', multiplier: 100000000, trailer: ' Satoshis'},
	]),
	
	selectedVCurrency: null,
});