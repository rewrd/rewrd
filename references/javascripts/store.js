Rewrd.ApplicationAdapter = DS.RESTAdapter.extend({
	//bulkCommit: false,
	host: 'http://www.rewrd.com',
	namespace: 'api/',
	
	headers: {
    'HTTP_AUTH_LOGIN': $('meta[name="an:webstore"]').attr('content'),
		'HTTP_AUTH_PASSWD': $('meta[name="an:sessions"]').attr('content'),
		'X_ANAMO_LANG': $('html').attr('lang')
  },
	
	pathForType: function(type) {
    var underscored = Em.String.dasherize(type);
    return Em.String.pluralize(underscored);
  },

	ajaxError: function(jqXHR) { // http://emberjs.com/api/data/classes/DS.InvalidError.html
    var error = this._super(jqXHR);

    if(jqXHR && jqXHR.status === 422) {
      return new DS.InvalidError(Em.$.parseJSON(jqXHR.responseText)['errors']);
    } else {
      return error;
    }
  }
});

Rewrd.ApplicationSerializer = DS.RESTSerializer.extend({
  serializeIntoHash: function(data, type, record, options) {
    var root = Em.String.dasherize(type.typeKey);
    data[root] = this.serialize(record, options);
  }
});

Rewrd.inject('component', 'store', 'store:main');