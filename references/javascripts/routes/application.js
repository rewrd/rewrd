Em.Route.extend({
	beforeModel: function() {
		this.controllerFor('application').set('currencies', this.store.find('currency'));
	},
	
	actions: {
    didTransition: function() {
      $(window).scrollTop(0);
			if(!Em.isEmpty(Em.View.views['application-view'])) {
				Em.View.views['application-view'].set('selectCoin', false);
			}
      return true;
    }
  }
});