<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */
require_once 'references/index.php';
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Blob\Models\CreateBlobOptions;
#Public Class
	/**
	 * GIT DEPLOYMENT SCRIPT
	 *
	 * Used for automatically deploying websites via github or bitbucket, more deets here:
	 *
	 *		https://gist.github.com/1809044
	 */
 
	// The commands
	$commands = array(
		'cd $PWD',
		'whoami',
		'git pull',
		'git status',
		'git submodule sync',
		'git submodule update',
		'git submodule status',
	);
 
	// Run the commands for output
	$output = '';
	foreach($commands as $command){
		// Run it
		$tmp = shell_exec($command);
		// Output
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
	}

// azure stuff
$blobRestProxy = ServicesBuilder::getInstance()->createBlobService('DefaultEndpointsProtocol=http;AccountName=rewrd;AccountKey=YD/PVeeEGZSy46ENW0sOKD7CyjZwSwBdza8ostgDyx5rPG7mAPVc+F1kQ70r5Y/+ycKQbV+z5+91R34VFlyqwg==');

// List blobs
$blob_list = $blobRestProxy->listBlobs('assets');
$blobs = $blob_list->getBlobs();
foreach($blobs as $blob) {
	$blobRestProxy->deleteBlob('assets', $blob->getName());
}

foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(realpath('references/assets'))) as $filename => $file) {
	if($file->isDir()) {
		continue;
	}
	$blobRestProxy->createBlockBlob('assets', $file->getBaseName(), fopen($file->getPathName(), 'r'));
}

// js
$fileContents = file_get_contents('references/javascripts/app.js');
$GLOBALS['Rewrd'] = new Ember\Application('Rewrd');
array_walk(glob('references/javascripts/models/*.php'), function($file, $index) {
	require_once $file;
});
$fileContents .= $Rewrd->toJavascriptModel();
$allPaths = array_merge(
	glob('references/javascripts/*.js'),
	glob('references/javascripts/components/*.js'),
	glob('references/javascripts/controllers/*.js'),
	glob('references/javascripts/helpers/*.js'),
	glob('references/javascripts/lib/*.js'),
	glob('references/javascripts/mixins/*.js'),
	glob('references/javascripts/models/*.js'),
	glob('references/javascripts/routes/*.js'),
	glob('references/javascripts/views/*.js')
);
$priorityPaths = array_merge(
	glob('references/javascripts/lib/*.js'),
	glob('references/javascripts/*.js'),
	glob('references/javascripts/models/*.js'),
	glob('references/javascripts/mixins/*.js'),
	glob('references/javascripts/components/*.js'),
	glob('references/javascripts/routes/*.js'),
	glob('references/javascripts/views/*.js'),
	glob('references/javascripts/controllers/*.js'),
	glob('references/javascripts/helpers/*.js')
);
array_walk(array_merge(array_intersect($allPaths, $priorityPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents) {
	if(stripos($file, 'mixins') !== false) {
		$fileContents .= "\nRewrd.".Ember\String\classify(basename($file, '.js')).'=';
	} elseif(stripos($file, 'components') !== false) {
		$fileContents .= "\nRewrd.".Ember\String\classify(basename($file, '.js')).'Component=';
	} elseif(stripos($file, 'routes') !== false) {
		$fileContents .= "\nRewrd.".Ember\String\classify(basename($file, '.js')).'Route=';
	} elseif(stripos($file, 'views') !== false) {
		$fileContents .= "\nRewrd.".Ember\String\classify(basename($file, '.js')).'View=';
	} elseif(stripos($file, 'controllers') !== false) {
		$fileContents .= "\nRewrd.".Ember\String\classify(basename($file, '.js')).'Controller=';
	} elseif(stripos($file, 'helpers') !== false) {
		$fileContents .= "\nEm.Handlebars.registerBoundHelper('".basename($file, '.js')."',";
	}
	if(!mb_strends($file, 'references/javascripts/app.js')) {
		$fileContents .= file_get_contents($file);
	}
	if(stripos($file, 'helpers') !== false) {
		$fileContents .= ");";
	}
});
$options = new CreateBlobOptions();
$options->setBlobContentType('application/x-javascript');
$blobRestProxy->createBlockBlob('assets', substr($_SERVER['SCRIPT_NAME'], 1).'.js', $fileContents, $options);

// css
$fileContents = '';
$allPaths = array_merge(array(), glob('references/stylesheets/*.css'));
$priorityPaths = array();
array_walk(array_merge(array_intersect($allPaths, $priorityPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents) {
	$fileContents .= file_get_contents($file);
});
$options = new CreateBlobOptions();
$options->setBlobContentType('text/css');
$blobRestProxy->createBlockBlob('assets', substr($_SERVER['SCRIPT_NAME'], 1).'.css', $fileContents, $options);

	// Make it pretty for manual user access (and why not?)
#End Class
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
 .  ____  .    ____________________________
 |/      \|   |                            |
[| <span style="color: #FF0000;">&hearts;    &hearts;</span> |]  | Git Deployment Script v0.1 |
 |___==___|  /              &copy; oodavid 2012 |
              |____________________________|
 
<?php echo $output; ?>
</pre>
</body>
</html>