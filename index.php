<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */
require_once 'references/index.php';
#Public Class

// Maintainance
if(ip() != '94.68.196.15') {
	die('<img src="https://anamo.blob.core.windows.net/img3/maintainance.jpg"/>');
}

// hl cookie
$detectedLocale = Locale::acceptFromHttp((isset($_GET['hl'])? $_GET['hl']: (!empty($_SESSION['7018a8e5-3872-4e97-87f4-fc88c3ec97de'])? $_SESSION['7018a8e5-3872-4e97-87f4-fc88c3ec97de']: (!empty($_COOKIE['7018a8e5-3872-4e97-87f4-fc88c3ec97de'])? $_COOKIE['7018a8e5-3872-4e97-87f4-fc88c3ec97de']: $_SERVER['HTTP_ACCEPT_LANGUAGE']))));
if(empty($detectedLocale)) {// || !is_dir(stream_resolve_include_path('resbundles/'.$detectedLocale))) {
	$detectedLocale = \Locale::composeLocale(array('language' => 'en', 'region' => 'US'));
}
$_SESSION['7018a8e5-3872-4e97-87f4-fc88c3ec97de'] = $detectedLocale;
setcookie('7018a8e5-3872-4e97-87f4-fc88c3ec97de', $detectedLocale, (new DateTime)->add(new DateInterval('P180D'))->getTimestamp(), '/');
if(isset($_GET['hl'])) {
	r($_SERVER['REDIRECT_URL']);
}

// locale
$locale = \Locale::parseLocale($detectedLocale.'.utf8');

$myNumberFormatter = new \NumberFormatter(Locale::composeLocale($locale), \NumberFormatter::DECIMAL);
$myMoneyFormatter = new \NumberFormatter(Locale::composeLocale($locale), \NumberFormatter::CURRENCY);

setlocale(LC_MESSAGES, Locale::composeLocale($locale));
//bindtextdomain('cart', realpath(stream_resolve_include_path('resbundles')));
//textdomain('cart');
//bind_textdomain_codeset('cart', 'UTF-8');


#End Class
?><!DOCTYPE html>
<!--[if IE 8]> <html class="no-js ie ie-legacy ie8" lang="<?php echo Locale::composeLocale($locale); ?>"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie ie-legacy ie9" lang="<?php echo Locale::composeLocale($locale); ?>"> <![endif]-->
<!--[if !(IE)]><!-->
<html class="light no-js country-<?php echo \Locale::getRegion(\Locale::composeLocale($locale)); ?> lang-<?php echo \Locale::getPrimaryLanguage(\Locale::composeLocale($locale)); ?>" lang="<?php echo Locale::composeLocale($locale); ?>">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="Rewrd">

<meta name="rw:guessed_referer" content="<?php echo htmlspecialchars($_SERVER['HTTP_REFERER']); ?>">
<meta name="rw:guessed_locale" content="<?php echo htmlspecialchars(\Locale::composeLocale($locale)); ?>">
<meta name="rw:guessed_lang" content="<?php echo htmlspecialchars(\Locale::getPrimaryLanguage(\Locale::composeLocale($locale))); ?>">
<meta name="rw:guessed_region" content="<?php echo htmlspecialchars(\Locale::getRegion(\Locale::composeLocale($locale))); ?>">
<meta name="rw:currencies" content="<?php
$locales = array();
foreach(IntlCalendar::getAvailableLocales() as $locale2) {
	if(empty(Locale::getDisplayRegion($locale2, 'en'))) {
		continue;
	}
	//if($mft1->getSymbol(NumberFormatter::CURRENCY_SYMBOL) != 'EUR') {
//		continue;
//	}
	$mft1 = new \NumberFormatter($locale2, \NumberFormatter::CURRENCY);
	array_push($locales, array(
		'locale' => $locale2,
		'region' => Locale::getDisplayRegion($locale2, 'en'),
		'symbol' => $mft1->getSymbol(NumberFormatter::CURRENCY_SYMBOL),
		'iso' => $mft1->getSymbol(NumberFormatter::INTL_CURRENCY_SYMBOL),
		'format' => str_replace('¤', '%s', str_replace('-', '%v', preg_replace('/[\#\,\.0-9]+/', '-', $mft1->getPattern()))),
		'decimal' => $mft1->getSymbol(NumberFormatter::DECIMAL_SEPARATOR_SYMBOL),
		'thousand' => $mft1->getSymbol(NumberFormatter::MONETARY_SEPARATOR_SYMBOL),
		'precision' => $mft1->getAttribute(NumberFormatter::FRACTION_DIGITS)
	));
}
echo htmlspecialchars(json_encode($locales)); ?>">

<link rel="stylesheet" type="text/css" href="//rewrd.blob.core.windows.net/assets/bec0248b-0d1d-4706-a410-3ced527b0aeb.css">
<link rel="shortcut icon" type="image/x-icon" href="//rewrd.blob.core.windows.net/assets/rewrd_favicon.png">

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.3.0/handlebars.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.5.3/numeral.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ember.js/1.6.1/ember.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ember-data.js/1.0.0-beta.8/ember-data.min.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCInfutA17fcRqMkltNM3pHUUUmG2EsnTI&language=<?php echo \Locale::getPrimaryLanguage(\Locale::composeLocale($locale)); ?>&region=<?php echo \Locale::getRegion(\Locale::composeLocale($locale)); ?>&sensor=false"></script>
<script src="//rewrd.blob.core.windows.net/assets/bec0248b-0d1d-4706-a410-3ced527b0aeb.js"></script>
<script>
moment.lang($('html').attr('lang'));
accounting.settings = {currency:{symbol:'<?php echo $myMoneyFormatter->getSymbol(NumberFormatter::CURRENCY_SYMBOL); ?>',format:'<?php echo str_replace('¤', '%s', str_replace('-', '%v', preg_replace('/[\#\,\.0-9]+/', '-', $myMoneyFormatter->getPattern()))); ?>',decimal:'<?php echo $myMoneyFormatter->getSymbol(NumberFormatter::DECIMAL_SEPARATOR_SYMBOL); ?>',thousand:'<?php echo $myMoneyFormatter->getSymbol(NumberFormatter::MONETARY_SEPARATOR_SYMBOL); ?>',precision:2},number:{precision:2,thousand:'<?php echo $myNumberFormatter->getSymbol(NumberFormatter::GROUPING_SEPARATOR_SYMBOL); ?>',decimal:'<?php echo $myMoneyFormatter->getSymbol(NumberFormatter::DECIMAL_SEPARATOR_SYMBOL); ?>'}};
</script>
<title>Rewrd</title>
</head>

<body>
<?php // include all templates
array_walk(glob('references/templates/*.html'), function($file, $index) {
	echo '<script type="text/x-handlebars" data-template-name="'.str_replace('.', '/', basename($file, '.html')).'">';
	require($file);
	echo '</script>';
});
?>
</body>
</html>