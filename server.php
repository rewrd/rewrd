<?php /* Copyright 2006-2014 Panagiotis Tsimpoglou. All rights reserved. */
require_once 'references/index.php';//ini_set('error_reporting', E_ALL);
require_once 'references/scripts/Rewrd.php';
#Public Class
header('Content-type: application/json');


$db1 = new Rewrd\SqlDataSource();


$GLOBALS['Rewrd'] = new Ember\Application;
array_walk(glob('references/javascripts/models/*.php'), function($file, $index) {
	require_once $file;
});


$path = array_filter(explode('/', trim($_REQUEST['q'], '/')));
$params = array_diff($_REQUEST, array('q' => $_REQUEST['q']));


/**
	//Action HTTP Verb URL
	//Find GET /people/123
	//Find All GET /people
	//Update PUT /people/123
	//Create POST /people
	//Delete DELETE /people/123
	*/
switch(mb_strtolower($_SERVER['REQUEST_METHOD'])) {
	case 'delete':
		$id = array_pop($path);
		
		try {
			require_once 'references/server/'.implode('/', $path).'.delete.php';
			
			header($_SERVER['SERVER_PROTOCOL'].' 204 No Content');
			
		} catch(Exception $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
			if(!empty($e->getMessage())) {
				die(json_encode((object)array(
					'errors' => (object)array(
						$e->getMessage()
					)
				)));
			}
		}
		
		break;
		
	case 'put':
		$id = array_pop($path);
		$rawInput = json_decode(file_get_contents('php://input'))->{Ember\String\singularize(Ember\String\dasherize(first($path)))};
		
		try {
			$errors = $App->{Ember\String\singularize(Ember\String\classify(first($path)))}->validate($rawInput);
			if(count($errors) > 0) {
				throw new Exception(NULL, 422);
			}
			
			$dbInput = $App->{Ember\String\singularize(Ember\String\classify(first($path)))}->db($rawInput);
			
			require_once 'references/server/'.implode('/', $path).'.update.php';
	
			require 'references/server/'.first($path).'.find.php';
			
		} catch(DS\ValidationError $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 422 Unprocessable Entity');
			die(json_encode((object)array(
				'errors' => (object)array(
					$e->getModelProperty() => array($e->getMessage())
				)
			)));
		} catch(Exception $e) {
			if($e->getCode() == 422) {
				header($_SERVER['SERVER_PROTOCOL'].' 422 Unprocessable Entity');
				die(json_encode((object)array(
					'errors' => $errors
				)));
			} else {
				header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
			}
		}
		
		break;
		
	case 'get':
		if(count($path) == 1) {
			$json = array(
				'meta' => array(
					'total' => 0
				)
			);
			
			require_once 'references/server/'.implode('/', $path).'.findall.php';
			
			echo json_encode($json);
			
		} else {
			$id = array_pop($path);
			$rawInput = json_decode(file_get_contents('php://input'));
			
			try {
				require_once 'references/server/'.implode('/', $path).'.find.php';
				
			} catch(Exception $e) {
				header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
			}
		}
		
		break;
		
	case 'post':
		$rawInput = json_decode(file_get_contents('php://input'))->{Ember\String\singularize(Ember\String\dasherize(first($path)))};
		
		try {
			$errors = $App->{Ember\String\singularize(Ember\String\classify(first($path)))}->validate($rawInput);
			if(count($errors) > 0) {
				throw new Exception(NULL, 422);
			}
			
			$dbInput = $App->{Ember\String\singularize(Ember\String\classify(first($path)))}->db($rawInput);
			
			require_once 'references/server/'.implode('/', $path).'.create.php';
			
			require 'references/server/'.first($path).'.find.php';
			
		} catch(DS\ValidationError $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 422 Unprocessable Entity');
			die(json_encode((object)array(
				'errors' => (object)array(
					$e->getModelProperty() => array($e->getMessage())
				)
			)));
		} catch(Exception $e) {
			if($e->getCode() == 422) {
				header($_SERVER['SERVER_PROTOCOL'].' 422 Unprocessable Entity');
				die(json_encode((object)array(
					'errors' => $errors
				)));
			} else {
				header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
			}
		}
		
		break;
	
	default:
		header($_SERVER['SERVER_PROTOCOL'].' 405 Method Not Allowed');
		die();
}


#End Class
?>